-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2024 at 09:41 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thuvien`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) NOT NULL,
  `login_id` varchar(20) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `actived_flag` int(1) DEFAULT 1,
  `reset_password_token` varchar(100) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `login_id`, `password`, `actived_flag`, `reset_password_token`, `updated`, `created`) VALUES
(1, 'bach', 'fcea920f7412b5da7be0cf42b8c93759', 1, '', '2024-01-04 15:22:30', '2023-12-20 00:20:14');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `category` char(10) DEFAULT NULL,
  `author` varchar(250) DEFAULT NULL,
  `quantity` int(3) DEFAULT NULL,
  `avatar` varchar(250) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `name`, `category`, `author`, `quantity`, `avatar`, `description`, `updated`, `created`) VALUES
(1, 'Giải tích 1', 'Sách in', 'Author 1', 14, 'avatar_url_1', 'Description for Book 1', '2023-12-20 00:56:20', '2023-12-20 00:56:20'),
(2, 'Đại số', 'Sách bài t', 'Author 2', 15, 'avatar_url_2', 'Description for Book 2', '2023-12-20 00:56:20', '2023-12-20 00:56:20'),
(3, 'Giải tích 2', 'Sách in', 'Author 3', 19, 'avatar_url_3', 'Description for Book 3', '2023-12-20 00:56:20', '2023-12-20 00:56:20');

-- --------------------------------------------------------

--
-- Table structure for table `book_transactions`
--

CREATE TABLE `book_transactions` (
  `id` int(10) NOT NULL,
  `book_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `borrowed_date` datetime DEFAULT NULL,
  `return_plan_date` datetime DEFAULT NULL,
  `return_actual_date` datetime DEFAULT NULL,
  `description` text DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `book_transactions`
--

INSERT INTO `book_transactions` (`id`, `book_id`, `user_id`, `borrowed_date`, `return_plan_date`, `return_actual_date`, `description`, `updated`, `created`) VALUES
(40, 2, 2, '2023-12-30 00:47:00', '2023-12-19 12:52:50', '2023-12-30 02:47:00', NULL, NULL, NULL),
(41, 1, 1, '2023-12-30 00:48:00', '2023-12-19 12:52:50', '2023-12-31 04:48:00', NULL, NULL, NULL),
(42, 3, 3, '2023-12-30 00:49:00', '2023-12-19 12:52:50', '2024-01-03 00:49:00', NULL, NULL, NULL),
(43, 2, 1, '2023-12-31 00:49:00', '2023-12-19 12:52:50', '2024-01-01 03:49:00', NULL, NULL, NULL),
(44, 1, 6, '2023-12-30 00:53:00', '2023-12-19 12:52:50', '2023-12-31 00:53:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL COMMENT '1',
  `type` int(1) DEFAULT 1,
  `name` varchar(250) DEFAULT NULL,
  `user_id` char(15) DEFAULT NULL,
  `avatar` varchar(250) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `type`, `name`, `user_id`, `avatar`, `description`, `updated`, `created`) VALUES
(1, 1, 'Trần Xuân Bách', '1', '\"C:UsersHPPicturesweb.jpg\"', 'Mô tả về Trần Xuân Bách', '2023-12-20 00:17:59', '2023-12-20 00:17:59'),
(2, 1, 'Nguyen Hong Anh', '2', NULL, 'Mô tả', '2023-12-26 22:45:02', '2023-12-26 22:45:02'),
(3, 1, 'Bich', '4', NULL, NULL, NULL, NULL),
(5, 1, 'Thao', '5', NULL, NULL, NULL, NULL),
(6, 1, 'Duy', '6', '\r\n', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_transactions`
--
ALTER TABLE `book_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `book_transactions`
--
ALTER TABLE `book_transactions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '1', AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
