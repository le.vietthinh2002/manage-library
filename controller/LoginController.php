<?php
session_start();
require_once '/WEB/manage-library/model/UserModel.php';

$errors = [];

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Validate form data for login
    $username = $_POST["login_id"];
    $password = $_POST["password"];

    // Quy tắc validation
    if (empty($username)) {
        $errors[] = "Hãy nhập loginid.";
    } elseif (strlen($username) < 4) {
        $errors[] = "Loginid phải có ít nhất 4 ký tự.";
    }

    if (empty($password)) {
        $errors[] = "Hãy nhập password.";
    } elseif (strlen($password) < 6) {
        $errors[] = "Password phải có ít nhất 6 ký tự.";
    }

    // Kiểm tra lỗi
    if (empty($errors)) {
        $userModel = new UserModel(); // Tạo đối tượng UserModel
        $user = $userModel->login($username, $password);

        if ($user) {
            // Thực hiện đăng nhập thành công
            $_SESSION['login_id'] = $username;
             header("Location: home.php");
            exit;
        } else {
            $errors[] = "Loginid và password không đúng.";
        }
    }
}

include 'login.php';
?>
