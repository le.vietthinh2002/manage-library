<?php
include '/WEB/manage-library/connection.php';
        $user_id = $_POST['user_id'];

        // Thực hiện truy vấn kiểm tra trùng
        $stmt = $conn->prepare("SELECT COUNT(*) FROM users WHERE sub_id = ?");
        $stmt->bind_param('s', $user_id);
        $stmt->execute();
        $stmt->bind_result($result);
        $stmt->fetch();

        if ($result > 0) {
            echo 'duplicate';
        } else {
            echo 'not_duplicate';
        }
?>
