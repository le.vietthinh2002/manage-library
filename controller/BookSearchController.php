<?php

require_once '/WEB/manage-library/model/BookTransactionModel.php';
require_once "/WEB/manage-library/model/BookModel.php";
require_once "/WEB/manage-library/model/UserModel.php";

if (isset($_POST['search'])) {
    // Xử lý tìm kiếm
    $bookId = $_POST['book'];
    $userId = $_POST['user'];
    $searchResults = BookTransactionModel::searchTransactions(empty($bookId) ? null : $bookId, empty($userId) ? null : $userId);
} elseif (isset($_POST['reset'])) {
    $searchResults = BookTransactionModel::searchTransactions(null, null);
}

include 'Sach_Lichsumuon-view.php';
?>

