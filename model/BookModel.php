<?php
class BookModel {
    public static function getAllBooks() {
        // Giả sử $pdo là đối tượng PDO kết nối đến cơ sở dữ liệu
        $pdo = new PDO("mysql:host=localhost;dbname=library", "root", "");

        $stmt = $pdo->prepare("SELECT * FROM books");
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
?>