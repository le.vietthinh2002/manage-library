<?php
class UserModel {
    public static function getAllUsers() {
        // Giả sử $pdo là đối tượng PDO kết nối đến cơ sở dữ liệu
        $pdo = new PDO("mysql:host=localhost;dbname=library", "root", "");

        $stmt = $pdo->prepare("SELECT * FROM users");
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getLoginId($loginId) {
        // Giả sử $pdo là đối tượng PDO kết nối đến cơ sở dữ liệu
        $pdo = new PDO("mysql:host=localhost;dbname=library", "root", "");

        $stmt = $pdo->prepare("SELECT * FROM admins WHERE login_id = :login_id");
        $stmt->bindParam(':login_id', $loginId);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC); // Sử dụng fetch thay vì fetchAll vì bạn chỉ mong đợi một dòng dữ liệu
    }

    public static function login($loginId, $password) {
        $user = UserModel::getLoginId($loginId);

        if ($user) {
            $hashedPassword = md5($password);

            // So sánh mật khẩu
            if ($hashedPassword === $user['password']) {
                return true; // Đăng nhập thành công
            }
        }

        return false; // Đăng nhập thất bại
    }

    public static function loginIdExists($loginId) {
        $pdo = new PDO("mysql:host=localhost;dbname=library", "root", "");
        $stmt = $pdo->prepare("SELECT COUNT(*) FROM admins WHERE login_id = :login_id");
        $stmt->bindParam(':login_id', $loginId);
        $stmt->execute();
        $count = $stmt->fetchColumn();
        return $count > 0;
    }

    public static function updateResetToken($loginId, $resetToken) {
        $pdo = new PDO("mysql:host=localhost;dbname=library", "root", "");
        $stmt = $pdo->prepare("UPDATE admins SET reset_password_token = :reset_token WHERE login_id = :login_id");
        $stmt->bindParam(':reset_token', $resetToken);
        $stmt->bindParam(':login_id', $loginId);
        $stmt->execute();
    }

    public static function getLoginIdByResetToken($loginId) {
        $pdo = new PDO("mysql:host=localhost;dbname=library", "root", "");
        $stmt = $pdo->prepare("SELECT reset_password_token FROM admins WHERE login_id = :login_id");
        $stmt->bindParam(':login_id', $loginId);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public static function updatePassword($loginId, $hashedPassword) {
        $pdo = new PDO("mysql:host=localhost;dbname=library", "root", "");
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $updatedAt = date("Y-m-d H:i:s");
        $stmt = $pdo->prepare("UPDATE admins SET password = :hashed_password, updated = :updated_at WHERE login_id = :login_id");
        $stmt->bindParam(':hashed_password', $hashedPassword);
        $stmt->bindParam(':updated_at', $updatedAt);
        $stmt->bindParam(':login_id', $loginId);
        $stmt->execute();
    }

    public static function clearResetToken($loginId) {
        $pdo = new PDO("mysql:host=localhost;dbname=library", "root", "");
        $stmt = $pdo->prepare("UPDATE admins SET reset_password_token = '' WHERE login_id = :login_id");
        $stmt->bindParam(':login_id', $loginId);
        $stmt->execute();
    }
    
}
?>