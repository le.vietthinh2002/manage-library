<?php
class user_model {
    public function add_user() {
        include('/WEB/manage-library/connection.php');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $current_datetime = date("Y-m-d H:i:s");
        
        $stmt = $conn->prepare("INSERT INTO users (name, type, sub_id, avatar, description, created) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssss", $_SESSION["username"], $_SESSION["selected_type"], $_SESSION["user_id"], $_SESSION["target_file"], $_SESSION["description"], $current_datetime);
        
        if ($stmt->execute()) {
            echo "Đã đăng kí thành công";
        } else {
            echo "Lỗi: " . $stmt->error;
        }

        $stmt->close();
        $conn->close();
    }

    public function edit_user($user_id) {
        include('/WEB/manage-library/connection.php');
        
        $stmt = $conn->prepare("UPDATE users SET name = ?, type = ?, avatar = ?, description = ?, updated = ? WHERE sub_id = ?");
        $stmt->bind_param("ssssss", $_SESSION["username"], $_SESSION["selected_type"], $_SESSION["target_file"], $_SESSION["description"], date("Y-m-d H:i:s"), $user_id);
        if ($stmt->execute()) {
            echo "Đã đăng kí thành công";
        } else {
            echo "Lỗi: " . $stmt->error;
        }
        $stmt->close();
        $conn->close();
    }
	public function get_user($user_id) {
		include('/WEB/manage-library/connection.php');
		$stmt = $conn->prepare("SELECT `id`, `type`, `name`, `sub_id`, `avatar`, `description`, `updated`, `created` FROM users WHERE `sub_id` = ?");

			$stmt->bind_param("s", $user_id);
			$stmt->execute();
			
			$result = $stmt->get_result();
			$row = $result->fetch_assoc();
			
			// Close the statement and connection
			$stmt->close();
			$conn->close();
			
			return $row;
	}
		
	
    public function search($keyword, $type) {
        include('/WEB/manage-library/connection.php');
        $req = "select id, sub_id, name, `type`, description from `users` where (name like '%$keyword%' or description like '%$keyword%')";
			if($type !== ''){
				$req = $req." and `type` = '$type'";
			}
			$resp = array("success"=>FALSE);
			try{
				$result = $conn->query($req);
				
				$resp["data"] = array();
				if ($result->num_rows > 0) {
				  while($row = $result->fetch_assoc()) {
					$type = "";
					if($row["type"] == 1){
						$type = "Giáo viên";
					}else if($row["type"] == 2){
						$type = "Học sinh";
					}
					
					$student = array(
						"id"=>$row["id"],
						"sub_id" => $row["sub_id"],
						"name"=>$row["name"],
						"type"=>$type,
						"description" => $row["description"]
					);
					array_push($resp["data"],$student);
				  }
				  $resp["success"] = TRUE;
				  return json_encode($resp, JSON_UNESCAPED_UNICODE);
				}
			} catch(Exception $e){
				$resp["success"] = FALSE;
				return json_encode($resp, JSON_UNESCAPED_UNICODE);
			}
			$conn->close();
			
    }
	
	public function del($id){
		include('/WEB/manage-library/connection.php');
		$id = $conn -> real_escape_string($id);
		
		$sql = "DELETE FROM users WHERE id=$id";
			
		echo $conn->query($sql);
		$conn->close();
	}

}
?>
